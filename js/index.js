$(function(){
	$("[data-toggle='toolstip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval:3500                        

	});

	
	$('#contacto').on('show.bs.modal', function (e) {
		  // el on siempre se escribe dentro del JQUERY( id de elemetos llamados dentro del parentesis #contactoBtn)para subscribir eventos ... e es el parametro que trae los elementos JQuery

		  // ésta sería la acción:
		 console.log('El modal se está mostrando');// escribe en la consola de desarrollador
		$('#contactoBtn').removeClass('btn-outline-success');
		$('#contactoBtn').addClass('btn-warning');
		$('#contactoBtn').prop('disabled',true);    // desactivamos el botón

	});
	$('#contacto').on('shown.bs.modal', function (e) {

		  console.log('El modal se mostró'); 
	});

	$('#contacto').on('hide.bs.modal', function (e) {

		  console.log('El modal se oculta..'); 
		  
	}); 
	$('#contacto').on('hidden.bs.modal', function (e) {
		$('#contactoBtn').prop('disabled',false);       // activamos nuevamente el botón
		$('#contactoBtn').removeClass('btn-warning');
			$('#contactoBtn').addClass('btn-outline-success'); // regresamos a los colores originales, podemos dejarlo un color que indique que ya el usuario activó éste botón(tipo busqueda web)

		  console.log('El modal se ocultó'); 
		  
	});    		
});
